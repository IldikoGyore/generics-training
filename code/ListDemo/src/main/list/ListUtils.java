package list;


public class ListUtils {
	public static Object[] reverse(Object[] array) {
		Object[] result = new Object[array.length];
		for (int i = 0; i < array.length; i++) {
			result[array.length - 1 - i] = array[i];
		}
		return result;
	}

	public static <T> MinimaList<T> reverse(MinimaList<T> list) {
		MinimaList<T> result = new MinimaList<T>();
		for (int i = 0; i < list.size(); i++) {
			result.add(list.get(list.size() - 1 - i));
		}
		return result;
	}

	public static void inPlaceReverse(MinimaList<?> list) {
		doInPlaceReverse(list);
	}
	
	public static <T> void doInPlaceReverse(MinimaList<T> list) {
		MinimaList<T> tmp = reverse(list);
		list.clear();
		list.addAll(tmp);
	}


	public static <T extends Comparable<? super T>> T max(MinimaList<T> items) {
		
		T max = null;
		for (T item : items) {
			if (max == null ||  item.compareTo(max) > 0) {
				max = item;
			}
		}
		return max;
	}

}
