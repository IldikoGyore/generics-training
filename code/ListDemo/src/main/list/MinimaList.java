package list;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class MinimaList<T> implements Iterable<T> {
	//private final Class<T> type;
	private final T[] myArray = (T[]) new Object[100];
	private int myElementCount = 0;

//	public MinimaList(Class<T> type){ // ezzel le lehet kerni a generikus obj-nak a tipusat
//		this.type = type;
//	}
//	
//	public Class<T> getType() {
//		return type;
//	}

	public void add(T o) {
		myArray[myElementCount] = o;
		myElementCount++;
	}

	public void clear() {
		myElementCount = 0;
	}

	public void addAll(MinimaList<? extends T> otherList) {
//		for (Iterator<T> iterator = otherList.iterator(); iterator.hasNext();) {
//			T o = iterator.next();
//			add(o);
//		}
		for(T t: otherList){
			add(t);
		}
	}

	public boolean isEmpty() {
		return myElementCount == 0;
	}

	public int size() {
		return myElementCount;
	}

	public T get(int position) {
		return myArray[position];
	}

	@Override
	public Iterator<T> iterator() {
		return new SimpleIterator();
	}

	@Override
	public String toString() {
		StringBuilder s = new StringBuilder("[");
		Iterator<T> i = iterator();
		while (i.hasNext()) {
			s.append(i.next());
			if (i.hasNext()) {
				s.append(", ");
			}
		}
		s.append("]");
		return s.toString();
	}

	public T[] toArray() {
		T[] copy = (T[]) new Object[myElementCount];
		System.arraycopy(myArray, 0, copy, 0, myElementCount);
		return copy;
	}

	@Override
	public boolean equals(Object other) {
		boolean areTheSame = false;
		if (other instanceof MinimaList) {
			MinimaList<?> otherList = (MinimaList<?>) other;
			areTheSame = Arrays.deepEquals(this.myArray, otherList.myArray);
		}
		return areTheSame;
	}

	@Override
	public int hashCode() {
		int hash = myElementCount;
		for (int i = 0; i < myElementCount; i++) {
			Object item = myArray[i];
			if (item != null) {
				hash ^= item.hashCode();
			}
		}
		return hash;
	}

	public void sort(Comparator<? super T> comparator) {
		Arrays.sort(myArray, 0, myElementCount, comparator);
	}

	private class SimpleIterator implements Iterator<T> {
		private int myCurrentIndex = 0;

		@Override
		public boolean hasNext() {
			return myCurrentIndex < MinimaList.this.size();
		}

		@Override
		public T next() {
			if (!hasNext()) {
				throw new NoSuchElementException();
			}
			T item = get(myCurrentIndex);
			myCurrentIndex++;
			return item;
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}
	}
}
