package strategy.advanced;

public interface TaxStrategy <P extends TaxPayer>{
	long calculateTax(P p);
}
