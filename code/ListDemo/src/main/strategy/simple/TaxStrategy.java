package strategy.simple;

public interface TaxStrategy<T extends TaxPayer> {
	long calculateTax(T p);
}
