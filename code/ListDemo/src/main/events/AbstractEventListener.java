package events;

public abstract class AbstractEventListener<E extends Event> implements
		EventListener<E> {
	private final Class<E> eventType;

	protected AbstractEventListener(Class<E> eventType) {
		this.eventType = eventType;
	}

	@Override
	public Class<E> getHandledType() {
		return eventType;
	}

	@Override
	public void handleEventSimple(Event event) {
		handleEvent(eventType.cast(event));
	}

}
